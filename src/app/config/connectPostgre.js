const { Pool } = require("pg");
const connectionString = process.env.URL_POSTGRE;

exports.start = async function() {
  this.pool = new Pool({
    connectionString: connectionString
  });
};
exports.close = async function() {
  await this.pool.end();
};

exports.query = async function(q, data) {
  return this.pool.query(q, data);
};
