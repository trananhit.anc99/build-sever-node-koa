require('dotenv').config();
// Require db Postgre
const db = require('./config/connectPostgre');
// Require route
const router = require('./router');
const json = require('koa-json')
const Koa = require('koa');
const responseTime = require('koa-response-time');
const bodyParser = require('koa-bodyparser');
const app = new Koa();
// Connect postgre
// BodyParser
app.use(bodyParser({
    formidable:{uploadDir: './uploads'},
    multipart: true,
    urlencoded: true
 }));
app
    .use(router.routes())
    .use(router.allowedMethods())
    .use(json());

    
exports.start = async function () {
    try {
        await db.start();
        console.log('Database connect...');
        this.server = await app.listen(process.env.PORT || 5000, () => console.log('Serve run port: '+ process.env.PORT));
    } catch (error) {
        console.log(error);
    }
}
exports.close = async function () { 
    await this.server.close();
    console.log('Server close');
    await db.close();

 }


