const Router = require("koa-router");

const router = new Router();
// Require database
const database = require('./config/connectPostgre');
router
  .get("/", async(ctx, next) => {
      const showAllData = await database.query('SELECT * FROM person').then(a => a.rows);
      ctx.body = showAllData;
  })
  .post("/person", async(ctx, next) => {
    const {name, address} = ctx.request.body;
    // Insert data
    await database.query(`INSERT into person(name, address) VALUES('${name}', '${address}');`)
    // Get all data and print
  })
  .put("/person/:id", (ctx, next) => {
    const id = ctx.params.id;
    const { name, address } = ctx.request.body;
    ctx.body = id, name, address;
    database.query(`UPDATE person SET name='${name}', address='${address}' WHERE id=${id};`);
    
  })
  .del("/person/:id", (ctx, next) => {
    const id = ctx.params.id;
    database.query(`DELETE FROM person WHERE id=${id};`)
  })
  // .all("/person/:id", (ctx, next) => {});

module.exports = router;
